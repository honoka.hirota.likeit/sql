﻿USE
    item 
SELECT 
    item_category.category_name, 
    SUM(item.item_price)AS total_price 
FROM 
    item_category 
INNER JOIN 
    item
ON
    item.category_id = item_category.category_id
GROUP BY
    item_category.category_name
ORDER BY
    total_price DESC;

